@extends('public')

@section('content')
    <h1>Публикация текста</h1>
    {!! Form::open(['route' => 'text.store']) !!}
        @include('text._form')
    {!! Form::close()!!}
@endsection
