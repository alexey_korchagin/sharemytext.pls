@extends('public')
@section('content')
<h1>{{ $text->title }}</h1>
<h5>URL: {{ $url }}</h5>
@if ($text->access == 'unlist')
<div class="alert alert-warning" role="alert">
  Доступ только по прямой ссылке!
</div>
@endif
<pre class="bg-light my-3">
  <code>
    {{ $text->content }}
  </code>
</pre>

@stop
