<div class="row">
  <div class="col-12">
    <div class="form-group">
      {!! Form::label('Название') !!}
      {!! Form::text('title', null, ['class'=>'form-control'] ) !!}
    </div>
  </div>
  <div class="col-12">
    <div class="form-group">
      {!! Form::label('Текст') !!}
      {!! Form::textarea('content', null, ['class'=>'form-control'] ) !!}
    </div>
  </div>
  <div class="col-12 col-lg-6">
    {!! Form::label('', 'Видимость', array('class' => 'w-100')) !!}

    <div class="btn-group btn-group-toggle" data-toggle="buttons">
      <label class="btn btn-outline-primary active">
        {!! Form::radio('access','public', true) !!} Видно всем
      </label>
      <label class="btn btn-outline-primary">
        {!! Form::radio('access','unlist', false) !!} Доступ по ссылке
      </label>
    </div>
  </div>
  <div class="col-12 col-lg-6">
    {!! Form::label('Время действия') !!}
    {!! Form::select('expiration_time', array(
      'addMinutes' => '10 минут',
      'addHour' => '1 час',
      'addHours' => '3 часа',
      'addDay' => '1 день',
      'addWeek' => '1 неделя',
      'addMonth' => '1 месяц',
      'endless' => 'без ограничений'
    ), null,array('class'=>'form-control')); !!}
  </div>
</div>
<div class="form-group py-3">
  {!! Form::submit('Поделиться', ['class'=>'btn btn-success']) !!}
</div>
