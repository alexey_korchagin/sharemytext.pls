<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Text extends Model
{

  protected $fillable = [
    'title',
    'content',
    'user_id',
    'expiration_time',
    'access',
    'hash'
  ];

  public function scopeAvailable($query)
  {
    $query
    ->where('expiration_time', '>', Carbon::now())
    ->where('access', '=', 'public')
    ->take(10);
  }

  public function getTopTexts()
  {
    $texts = $this->latest('created_at')->available()->get();
    return $texts;
  }

  public function getText($hash)
  {
    $text = $this->where('hash', $hash)->first();
    return $text;
  }

}
