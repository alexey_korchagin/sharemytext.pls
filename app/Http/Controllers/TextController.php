<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Text;
use Carbon\Carbon;
use Auth;

class TextController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Text $textModel)
  {
    $texts = $textModel->getTopTexts();

    return view('text.index', ['texts' => $texts]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('text.create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $life_time = $request->expiration_time;

    if ($life_time != 'endless') {
      $now = Carbon::now();
      if ($life_time == 'addMinutes')
      {
        $request->expiration_time = $now->addMinutes(10);
      } elseif ($life_time == 'addHours') {
        $request->expiration_time = $now->addHours(3);
      } else {
        $request->expiration_time = $now->$life_time();
      }
    } else {
      $request->expiration_time = Carbon::create(9999);
    }

    $seed = str_split('abcdefghijklmnopqrstuvwxyz'
    .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    .'0123456789!@(_)');
    shuffle($seed);
    $hash = '';
    foreach (array_rand($seed, 7) as $k) $hash .= $seed[$k];

    $user_id = 0;
    if (Auth::check()) $user_id = Auth::user()->id;

    Text::create([
      'title' => $request->title,
      'content' => $request->content,
      'user_id' => $user_id,
      'expiration_time' => $request->expiration_time,
      'access' => $request->access,
      'hash' => $hash
    ]);
    return redirect()->action('TextController@show', [$hash]);
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($hash, Text $textModel,Request $request)
  {
    $text = $textModel->getText($hash);
    if ($text->expiration_time < Carbon::now()) {
      return redirect()->route('texts');
    }

    return view('text.show', ['text' => $text, 'url' => $request->url()]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
