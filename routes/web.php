<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TextController@index')->name('texts');
Route::get('/{text}', 'TextController@show')->name('show_text');
$router->resource('text', 'TextController');

Route::get('/user/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/user/login', 'Auth\LoginController@login');
Route::post('/user/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/user/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/user/register', 'Auth\RegisterController@register');

Route::post('/user/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/user/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('/user/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/user/password/reset', 'Auth\ResetPasswordController@reset');
