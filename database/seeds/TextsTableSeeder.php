<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Text;

class TextsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();
      Text::create([
        'title' => 'First Text',
        'content' => 'Content First Text body',
        'user_id' => 0,
        'expiration_time' => $now->addDay(),
        'access' => 'public',
        'hash' => 'giS3jsP'
      ]);
      Text::create([
        'title' => 'Second Text',
        'content' => 'Content Second Text body',
        'user_id' => 1,
        'expiration_time' => $now->addWeek(),
        'access' => 'public',
        'hash' => 'qulG5pW'
      ]);
      Text::create([
        'title' => 'Third Text',
        'content' => 'Content Third Text body',
        'user_id' => 1,
        'expiration_time' => $now->addDay(),
        'access' => 'public',
        'hash' => 'qMk9y3b'
      ]);
      Text::create([
        'title' => 'Fourth Text',
        'content' => 'Content Fourth Text body',
        'user_id' => 0,
        'expiration_time' => $now->addDay(),
        'access' => 'unlist',
        'hash' => '5_z!UkT'
      ]);
      Text::create([
        'title' => 'Fifth Text',
        'content' => 'Content Fifth Text body',
        'user_id' => 1,
        'expiration_time' => $now->addDay(),
        'access' => 'unlist',
        'hash' => 'sdkZl@7'
      ]);
      Text::create([
        'title' => 'Sixth Text',
        'content' => 'Content Sixth Text body',
        'user_id' => 1,
        'expiration_time' => $now->addSecond(),
        'access' => 'public',
        'hash' => 'pcjU!ie'
      ]);
    }
}
